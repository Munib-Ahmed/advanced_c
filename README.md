# Advanced_C++

clone this project into your system, using the following command;
```
git clone https://gitlab.com/Munib-Ahmed/advanced_c.git
``` 
Enter the cloned Directory, with:
```
cd advanced_c
```
Now, we need to create a directory typically named as "build"
```
mkdir build
```

navigate to build directory, 
```
cd build
```
Enter the following commands to make an executable;

```
cmake ..
cmake --build .
./Advanced_cpp
```
