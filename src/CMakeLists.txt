add_library(student_library STATIC student.cpp)
add_library(whitelist_library STATIC whitelist.cpp)

target_include_directories(student_library PRIVATE ${CMAKE_SOURCE_DIR}/include)
target_include_directories(whitelist_library PRIVATE ${CMAKE_SOURCE_DIR}/include)
